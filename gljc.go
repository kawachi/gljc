package main

import (
	"archive/zip"
	"bufio"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/go-git/go-git/v5"
)

func debug(format string, args ...interface{}) {
	log.Printf(format, args...)
}

type Record struct {
	Time     time.Time
	TreeHash string
	EnvHash  string
	JobId    string
}

func NewRecord(treeHash, envHash string) (*Record, error) {
	jobId, ok := os.LookupEnv("CI_JOB_ID")
	if !ok {
		return nil, fmt.Errorf("CI_JOB_ID not found")
	}

	return &Record{
		Time:     time.Now(),
		TreeHash: treeHash,
		EnvHash:  envHash,
		JobId:    jobId,
	}, nil
}

func (r *Record) IsSame(other *Record) bool {
	return r != nil && other != nil &&
		r.TreeHash == other.TreeHash &&
		r.EnvHash == other.EnvHash
}

const separator = "\t"

func ParseRecord(s string) (*Record, error) {
	arr := strings.SplitN(s, separator, 5)
	if len(arr) != 4 {
		return nil, fmt.Errorf("Invalid record: %d", len(arr))
	}
	unix, err := strconv.ParseInt(arr[0], 10, 64)
	if err != nil {
		return nil, err
	}
	return &Record{
		Time:     time.Unix(unix, 0),
		TreeHash: arr[1],
		EnvHash:  arr[2],
		JobId:    arr[3],
	}, nil
}

func (r *Record) String() string {
	arr := []string{strconv.FormatInt(r.Time.Unix(), 10), r.TreeHash, r.EnvHash, r.JobId}
	return strings.Join(arr, separator)
}

const maxRecords = 1000

//
// Usage:
//
// Job 成功を記録する
// $ gljc record CACHED_RECORD_FILE
//
// Job が過去に成功したか問い合わせる
// $ gljc find CACHED_RECORD_FILE && exit 0
//
func main() {
	tree, err := getTreeHash()
	if err != nil {
		log.Fatal(err)
	}

	env := getEnvHash()

	usage := "Usage: gljc (record|find) CACHED_RECORD_FILE"
	if len(os.Args) != 3 {
		log.Fatal(usage)
	}

	file := os.Args[2]
	if os.Args[1] == "record" {
		r, err := NewRecord(tree, env)
		if err != nil {
			log.Fatal(err)
		}
		err = record(file, r, maxRecords)
		if err != nil {
			log.Fatal(err)
		}
	} else if os.Args[1] == "find" {
		r, err := find(file, tree, env)
		if err != nil {
			log.Fatal(err)
		}
		if r != nil {
			fmt.Printf("The job has been successful at %s.\n", r.Time.Format(time.RFC822))
			fmt.Printf("TreeHash: %s, EnvHash: %s, jobId: %s\n", r.TreeHash, r.EnvHash, r.JobId)
			status, err := getArtifactStatus(r.JobId)
			if err != nil {
				log.Fatal(err)
			}
			debug("artifactStatus: %d", status)
			switch status {
			case NoArtifact:
				// Good. Do nothing
			case ArtifactExpired:
				log.Fatal("Artifact is expired.")
			case ArtifactExist:
				log.Printf("Downloading artifacts...\n")
				err = downloadArtifact(r.JobId)
				if err != nil {
					log.Fatal(err)
				}
				log.Printf("done\n")
			}
		} else {
			os.Exit(1)
		}
	} else {
		log.Fatal(usage)
	}
}

// HEAD の tree hash を得る
func getTreeHash() (string, error) {
	repo, err := git.PlainOpen(".")
	if err != nil {
		return "", fmt.Errorf("Current directory is not a git repository: %v", err)
	}
	head, err := repo.Head()
	if err != nil {
		return "", fmt.Errorf("Can't get HEAD: %v", err)
	}
	commit, err := repo.CommitObject(head.Hash())
	if err != nil {
		return "", fmt.Errorf("Can't get commit object: %v", err)
	}
	return commit.TreeHash.String(), nil
}

// Hash environmental variables.
func getEnvHash() string {
	h := sha1.New()
	environ := os.Environ()
	sort.Slice(environ, func(i, j int) bool { return environ[i] < environ[j] })
	for _, e := range environ {
		arr := strings.SplitN(e, "=", 2)
		key := arr[0]
		if isIgnoreEnv(key) {
			continue
		}
		fmt.Println(e)
		h.Write([]byte(e))
	}
	bs := h.Sum(nil)
	return fmt.Sprintf("%x", bs)
}

// We ignore all except CI_JOB_NAME
func isIgnoreEnv(name string) bool {
	return name != "CI_JOB_NAME"
}

func record(file string, r *Record, max int) error {
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	records, err := readRecords(f)
	if err != nil {
		return err
	}
	found := false
	for i, rec := range records {
		if rec.IsSame(r) {
			records[i] = r
			found = true
			break
		}
	}
	if !found {
		records = append(records, r)
	}

	over := len(records) - max
	if over > 0 {
		records = records[over:]
	}
	err = writeRecords(f, records)
	if err != nil {
		return err
	}
	return nil
}

func find(file, treeHash, envHash string) (*Record, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	records, err := readRecords(f)
	if err != nil {
		return nil, err
	}
	log.Printf("Searching treeHash: %s envHash: %s\n", treeHash, envHash)
	for _, r := range records {
		log.Printf("T: %s, E: %s\n", r.TreeHash, r.EnvHash)
		if r.TreeHash == treeHash && r.EnvHash == envHash {
			log.Printf("match\n")
			return r, nil
		}
	}
	return nil, nil
}

func readRecords(file *os.File) ([]*Record, error) {
	_, err := file.Seek(0, 0)
	if err != nil {
		return nil, err
	}
	scanner := bufio.NewScanner(file)
	records := []*Record{}
	for scanner.Scan() {
		line := scanner.Text()
		r, err := ParseRecord(line)
		if err != nil {
			log.Print("ERROR: Failed to parse record: ", err)
			continue
		}
		records = append(records, r)
	}
	if err := scanner.Err(); err != nil {
		scanner.Text()
		return nil, err
	}
	return records, nil
}

func writeRecords(file *os.File, records []*Record) error {
	_, err := file.Seek(0, 0)
	if err != nil {
		return err
	}
	err = file.Truncate(0)
	if err != nil {
		return err
	}
	writer := bufio.NewWriter(file)
	defer writer.Flush()
	for _, r := range records {
		_, err = writer.Write([]byte(r.String() + "\n"))
		if err != nil {
			return err
		}
	}
	return nil
}

type ArtifactStatus int

const (
	NoArtifact = iota
	ArtifactExist
	ArtifactExpired
)

// ref. https://docs.gitlab.com/ee/api/jobs.html#get-a-single-job
type Job struct {
	ArtifactExpireAt *string `json:"artifact_expire_at"`
	ArtifactsFile    *struct {
		Filename *string `json:"filename"`
		Size     int     `json:"size"`
	} `json:"artifacts_file"`
}

func getArtifactStatus(jobId string) (ArtifactStatus, error) {
	apiUrl, ok := os.LookupEnv("CI_API_V4_URL")
	if !ok {
		return 0, fmt.Errorf("CI_API_V4_URL is not set")
	}

	projectId, ok := os.LookupEnv("CI_PROJECT_ID")
	if !ok {
		return 0, fmt.Errorf("CI_PROJECT_ID is not set")
	}

	token, ok := os.LookupEnv("GLJC_TOKEN")
	if !ok {
		return 0, fmt.Errorf("GLJC_TOKEN is not set")
	}

	jobUrl := fmt.Sprintf("%s/projects/%s/jobs/%s", apiUrl, projectId, jobId)
	req, err := http.NewRequest("GET", jobUrl, nil)
	if err != nil {
		return 0, err
	}
	req.Header.Add("PRIVATE-TOKEN", token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return 0, fmt.Errorf("Failed to get job: %s, status: %d", jobUrl, resp.StatusCode)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	debug(string(body))
	var job Job
	if err := json.Unmarshal(body, &job); err != nil {
		return 0, err
	}
	if job.ArtifactsFile == nil || job.ArtifactsFile.Filename == nil {
		return NoArtifact, nil
	}
	if job.ArtifactExpireAt == nil {
		return ArtifactExist, nil
	}
	expire, err := time.Parse(time.RFC3339, *job.ArtifactExpireAt)
	if err != nil {
		return 0, err
	}
	// There's more than 5 mins to expire
	if time.Until(expire).Minutes() > 5.0 {
		return ArtifactExist, nil
	}

	return ArtifactExpired, nil
}

func downloadArtifact(jobId string) error {
	apiUrl, ok := os.LookupEnv("CI_API_V4_URL")
	if !ok {
		return fmt.Errorf("CI_API_V4_URL is not set")
	}

	projectId, ok := os.LookupEnv("CI_PROJECT_ID")
	if !ok {
		return fmt.Errorf("CI_PROJECT_ID is not set")
	}

	token, ok := os.LookupEnv("GLJC_TOKEN")
	if !ok {
		return fmt.Errorf("GLJC_TOKEN is not set")
	}

	url := fmt.Sprintf("%s/projects/%s/jobs/%s/artifacts", apiUrl, projectId, jobId)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	req.Header.Add("PRIVATE-TOKEN", token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf("Failed to get artifact: %s, status: %d", url, resp.StatusCode)
	}

	tmpfile, err := ioutil.TempFile("", "artifact")
	if err != nil {
		return err
	}
	defer os.Remove(tmpfile.Name())

	_, err = io.Copy(tmpfile, resp.Body)
	if err != nil {
		return err
	}

	tmpfile.Close()

	reader, err := zip.OpenReader(tmpfile.Name())
	if err != nil {
		return err
	}
	defer reader.Close()

	// https://stackoverflow.com/a/24792688
	// Closure to address file descriptors issue with all the deferred .Close() methods
	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				panic(err)
			}
		}()

		dest := "."
		path := filepath.Join(dest, f.Name)

		if f.FileInfo().IsDir() {
			os.MkdirAll(path, f.Mode())
		} else {
			os.MkdirAll(filepath.Dir(path), f.Mode())
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := f.Close(); err != nil {
					panic(err)
				}
			}()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}

	for _, f := range reader.File {
		err = extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}
