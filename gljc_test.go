package main

import (
	"testing"
)

func TestIsIgnoreCiJobName(t *testing.T) {
	if isIgnoreEnv("CI_JOB_NAME") {
		t.Fatal("CI_JOB_NAME should not be ignored")
	}
}

func TestIsIgnoreCiVars(t *testing.T) {
	if !isIgnoreEnv("CI_JOB_ID") {
		t.Fatal("CI_JOB_ID should be ignored")
	}
}

func TestIsIgnoreGljcToken(t *testing.T) {
	if !isIgnoreEnv("GLJC_TOKEN") {
		t.Fatal("GLJC_TOKEN should be ignored")
	}
}

func TestIsIgnoreHostname(t *testing.T) {
	if !isIgnoreEnv("HOSTNAME") {
		t.Fatal("HOSTNAME should be ignored")
	}
}
