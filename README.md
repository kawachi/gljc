# gljc: GitLab Job Cache

This project is currently a PoC.

# The Concept

When you merge MRs in GitLab, for example, multiple CI jobs are run on the same source code in It may be executed. To save time, if you expect a rerun to yield the same result for the same source code, you can skip executing the job.

gljc looks at the Git tree id and skips the job if it's been successfully executed before with the same tree id and job id.

At the end of the Job execution, the success is recorded and the Job success is included in the cache.

At the beginning of the Job execution, check if the record of success exists, and if so, exit 0.

At the end of the Job execution, the success is recorded and the Job success is included in the cache.

At the beginning of the Job execution, checks for the record of the success, and if it exists, exits 0.

If the job has been successful in the past, you can extract the artifacts from the past job and extract them. You need a private token from GitLab to do this.
Set a private token in the `GLJC_TOKEN` environment variable.

For Premium and higher plans, you can use `CI_JOB_TOKEN` instead of a private token.

